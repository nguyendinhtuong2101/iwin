<!DOCTYPE html>
<html lang="en">
<head>
  <title>Cấu hình</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<?php
    include 'db_conn.php';
    $sql = "SELECT * FROM config";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_assoc($result);
    //print_r($row['linkapp']);
 ?>
<div class="container">
  <h2 style="text-align:center; margin:30px">CẤU HÌNH HỆ THỐNG</h2>
  <form class="form-horizontal" action="save_config.php" method="post">
    <div class="form-group">
      <label class="control-label col-sm-2" for="linkgame">Link GAME</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="linkgame" name="linkgame" placeholder="Enter linkgame" value=<?php echo $row['linkgame']; ?>  >
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="giaitri">Link GIẢI TRÍ</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="giaitri" placeholder="Enter giải trí" name="giaitri" value=<?php echo $row['giaitri']; ?>>
      </div>
    </div>

     <div class="form-group">
      <label class="control-label col-sm-2" for="uu_dai">ƯU ĐÃI</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="uu_dai" placeholder="Enter ưu đãi" name="uu_dai" value=<?php echo $row['uu_dai']; ?>>
      </div>
    </div>
     <div class="form-group">
      <label class="control-label col-sm-2" for="hotro">HỖ TRỢ</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="hotro" placeholder="Enter hỗ trợ" name="hotro" value=<?php echo $row['hotro']; ?>>
      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-success">Lưu</button>
      </div>
    </div>
  </form>
</div>

</body>
</html>
