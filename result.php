<?php
	session_start();
	include 'db_conn.php';
	
	$empQuery = "SELECT * from config";	
	$resultData = mysqli_query($conn, $empQuery);
	
	while( $empRecord = mysqli_fetch_assoc($resultData) ) {
		$empData[] = $empRecord;
	}
	header('Content-Type: application/json');
	echo json_encode($empData);	

?>